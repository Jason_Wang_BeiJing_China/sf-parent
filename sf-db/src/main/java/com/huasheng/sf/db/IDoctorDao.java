package com.huasheng.sf.db;

import com.huasheng.sf.model.Doctor;
import com.huasheng.sf.model.exception.SFEntityNotFoundException;

public interface IDoctorDao {

	public Doctor getDoctorById(Long id) throws SFEntityNotFoundException;
}
