package com.huasheng.sf.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.huasheng.sf.db.IDoctorDao;
import com.huasheng.sf.model.Doctor;
import com.huasheng.sf.model.exception.SFEntityNotFoundException;
import com.huasheng.sf.service.IDoctorService;

@Service
public class DoctorService implements IDoctorService{

	private Log log = LogFactory.getLog(DoctorService.class);
	
	@Autowired
	private IDoctorDao doctorDao;
	
	@Override
	public Doctor getDoctorById(Long id) throws SFEntityNotFoundException {
		log.debug("GETTING DOCTOR : " + id);
		
		return doctorDao.getDoctorById(id);
	}

}
