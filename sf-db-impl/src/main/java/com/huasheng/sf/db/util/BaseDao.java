package com.huasheng.sf.db.util;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

public class BaseDao {

	private static String configName = "sf";
	
	@PersistenceContext(unitName="sf", type=PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;
	private LocalEntityManager localEntityManager;
	
	protected EntityManager getEntityManager(){
		EntityManager out = null;
		if(entityManager != null){
			out = entityManager;
		} else {
			out = getLocalEntityManager().get();
			if(out == null){
				out = createEntityManager();
				getLocalEntityManager().set(out);
			}
			if (!out.getTransaction().isActive()) {
				out.getTransaction().begin();
			}		
		}
		return out;
	}
	
	private LocalEntityManager getLocalEntityManager() {
		if (localEntityManager == null) {
			localEntityManager = new LocalEntityManager();
		}
		return localEntityManager;
	}
	
	private EntityManager createEntityManager() {
		EntityManager out = Persistence.createEntityManagerFactory(getConfigName()).createEntityManager();
		if (!out.getTransaction().isActive()) {
			out.getTransaction().begin();
		}
		return out;
	}
	
	public static String getConfigName() {
		return configName;
	}

	public static void setConfigName(String configName) {
		BaseDao.configName = configName;
	}

	private class LocalEntityManager extends ThreadLocal<EntityManager> {

		@Override
		public EntityManager get() {
			return super.get();
		}

		@Override
		public void remove() {
			super.remove();
		}

		@Override
		public void set(EntityManager value) {
			super.set(value);
		}
	}
}
