package com.huasheng.sf.db.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.huasheng.sf.db.IDoctorDao;
import com.huasheng.sf.db.util.BaseDao;
import com.huasheng.sf.model.Doctor;
import com.huasheng.sf.model.exception.SFEntityNotFoundException;
import com.huasheng.sf.pojos.DoctorPojo;

@Repository
public class DoctorDao extends BaseDao implements IDoctorDao {

	private Log log = LogFactory.getLog(DoctorDao.class);
	
	private DoctorPojo getDoctorPojo(Long id){
		return getEntityManager().find(DoctorPojo.class, id);
	}
	
	@Override
	public Doctor getDoctorById(Long id) throws SFEntityNotFoundException {
		DoctorPojo pojo = getDoctorPojo(id);
		if(pojo==null){
			log.debug("DOCTOR NOT FOUND FOR : " + id);
			throw new SFEntityNotFoundException("DOCTOR_NOT_FOUND", id);
		}
		Doctor out = new Doctor();
		out.setId(pojo.getId());
		out.setName(pojo.getName());
		return out;
	}

}
