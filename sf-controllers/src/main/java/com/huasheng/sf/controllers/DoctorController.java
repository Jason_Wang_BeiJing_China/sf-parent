package com.huasheng.sf.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.huasheng.sf.controllers.namespace.IDoctorNameSpace;
import com.huasheng.sf.model.Doctor;
import com.huasheng.sf.model.exception.SFEntityNotFoundException;
import com.huasheng.sf.service.IDoctorService;

/**
 * rest API for doctor : create update delete read list
 * @author wangshujie
 *
 */

@Controller
@RequestMapping(value=IDoctorNameSpace.DOCTOR_URI)
public class DoctorController {

	@Autowired
	private IDoctorService doctorService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Doctor getDoctor(@PathVariable Long id, HttpServletRequest request, HttpServletResponse response) throws SFEntityNotFoundException{
		return doctorService.getDoctorById(id);
	}
}
