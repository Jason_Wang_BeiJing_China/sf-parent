package com.huasheng.sf.controllers.namespace;

public interface IDoctorNameSpace extends IRootNameSpace{

	public static final String DOCTOR_NAMESPACE = "doctor";
	
	public static final String DOCTOR_URI=ROOT_URI + "/" + DOCTOR_NAMESPACE;
}
