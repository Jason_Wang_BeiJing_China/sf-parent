package com.huasheng.sf.controllers.advice;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.huasheng.sf.model.exception.SFBaseException;
import com.huasheng.sf.model.exception.SFEntityNotFoundException;
import com.huasheng.sf.model.exception.SFException;

@ControllerAdvice
@Order(value=Ordered.LOWEST_PRECEDENCE)
public class SFExceptionAdvice {

	private SFException generateSFException(Throwable e){
		SFException out = new SFException();
		if(e instanceof SFBaseException){
			SFBaseException sfe = (SFBaseException)e;
			out.setErrorCode(sfe.getCode());
			out.setErrorMessage(sfe.getMessage());
			out.setStackTrace(ExceptionUtils.getStackTrace(sfe));
		}
		return out;
	}
	
	@ExceptionHandler(value=SFEntityNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public SFException handleSFEntityNotFoundException(Throwable e){
		return generateSFException(e);
	}
}
