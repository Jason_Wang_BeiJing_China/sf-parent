package com.huasheng.sf.service;

import com.huasheng.sf.model.Doctor;
import com.huasheng.sf.model.exception.SFEntityNotFoundException;

public interface IDoctorService {

	public Doctor getDoctorById(Long id) throws SFEntityNotFoundException;
}
