package com.huasheng.sf.model;

import java.io.Serializable;

public class Doctor implements Serializable{
	
	private static final long serialVersionUID = -2891465581367557290L;
	private Long id;
	private String name;
	
	public Doctor(){
		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
