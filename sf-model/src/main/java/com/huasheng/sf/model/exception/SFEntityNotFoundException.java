package com.huasheng.sf.model.exception;

public class SFEntityNotFoundException extends SFBaseException{

	private static final long serialVersionUID = 3001705479661732396L;

	public SFEntityNotFoundException(String code, Object... params) {
		super(code, params);
	}
	
	
}
