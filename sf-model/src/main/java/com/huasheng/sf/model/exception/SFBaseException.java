package com.huasheng.sf.model.exception;

public class SFBaseException extends Exception {

	private static final long serialVersionUID = 5259832249903387931L;

	private String code;
	private Object[] params;
	
	public SFBaseException(){
		super();
	}
	
	public SFBaseException(String code, Object... params){
		super();
		this.code = code;
		this.params = params;
	}

	public SFBaseException(String code, Throwable e, Object... params){
		super(e);
		this.code = code;
		this.params = params;
	}
	
	public String getCode() {
		return code;
	}
	
	//TODO 如果需要实现异常信息国际化，扩展次方法取国际化Message
	public String getMessage(){
		StringBuilder out = new StringBuilder(getCode());
		if(params!=null){
			for(Object o:params){
				out.append(" : " + o.toString());
			}
		}
		return out.toString();
	}
	
}
