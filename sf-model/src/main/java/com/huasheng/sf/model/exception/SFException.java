package com.huasheng.sf.model.exception;

import java.io.Serializable;

public class SFException implements Serializable{

	/**
	 * @author wangshujie
	 */
	private static final long serialVersionUID = 3155928310314630784L;

	private String errorCode;
	private String errorMessage;
	private String stackTrace;
	
	public SFException() {
		super();
	}
	public SFException(String errorCode, String errorMessage, String stackTrace) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.stackTrace = stackTrace;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getStackTrace() {
		return stackTrace;
	}
	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}
	
	
	
}
